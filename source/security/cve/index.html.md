---
layout: markdown_page
title: CVE Numbering Authority
---

## CVEs and CNAs

CVEs ([Common Vulnerability Enumeration)](https://cve.mitre.org/index.html) are
unique identifiers assigned to specific vulnerabilities within a product,
having the form `CVE-YYYY-NNNNN`, with `YYYY` being the year and `NNNNN` being
a unique number for that year.

CNAs ([CVE Numbering Authorities](https://cve.mitre.org/cve/cna.html)) may
issue CVE identifiers to vulnerabilities in projects within the
CNA's scope. To obtain a CVE identifier for an identified vulnerability within
a CNA's scope, parties must contact the CNA and request a CVE identifier for the
vulnerability.

## GitLab's Role as a CNA

GitLab [is a participant in MITRE's CNA program](https://cve.mitre.org/cve/request_id.html#g).

GitLab's scope as a CNA is:

> The GitLab application, any project hosted on GitLab.com in a *public*
> repository, and any vulnerabilities discovered by GitLab that are not in
> another CNA’s scope

CVEs that have been assigned and published by GitLab can be found in the
[gitlab-org/cves project](https://gitlab.com/gitlab-org/cves).

## Requesting a CVE from GitLab

Maintainers of public projects hosted on GitLab.com may request CVEs for
vulnerabilities within their project by creating a
[confidential issue on gitlab-org/cves](https://gitlab.com/gitlab-org/cves/-/issues/new?issue[confidential]=true&issuable_template=Manual%20Submission&issue[title]=Vulnerability%20Submission)
for their CVE request.

Non-maintainers must work with the maintainer of the project to request a CVE
for a vulnerability. It is recommended that a confidential issue first be created
on the project itself to report the vulnerability to the maintainer. The
maintainer of the project is responsible for requesting the CVE identifier from
GitLab.

We will acknowledge receipt of CVE requests the next business day
and strive to send regular updates about our progress. Our goal is to
determine if the vulnerability is valid and communicate back to the submitter
within 30 business days.

If the status of the CVE request is unclear, please feel free to ping us
(`@gitlab-org/secure/vulnerability-research`) within the created issue. If
encrypted email is preferred, download our key from
the [MIT PGP key server](https://pgp.mit.edu/pks/lookup?op=get&search=0xFA6DEFCB219262C8)
or find it [below](#cve-public-gpg-key), and email us at `cve@gitlab.com`.

## CVE Public GPG Key

 * `GitLab CVE <cve@gitlab.com>`
 * ID: FA6DEFCB219262C8
 * Fingerprint 8DEEE034553BF98F760CA9D2FA6DEFCB219262C8

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF653qgBEACuTDL3NDNhJpXrZAb1eS1Lf29zGx5dXYSikjNEZiTlLJ12AYPC
x0bBxQnRrUuGvjWp1YKjKfD41cNXo5T3bjRmp3hFHJp9jahrbHmYP7etsoyxB+vu
h3h/ZimqqKzZpK0wMv8CCAEOgwnWOEaeI5ktiKrfV4BM3GcTiJ2a2rPwV/IkUc6I
K+pm5QEmqhrrfqP15qTs/CRC+vthaWz3IX9l1WgTo6y4nu2p6ESJdc+6W9Xe3Af2
EVu24m1gMgvbcay5vQpY5yH+9C1Bxs0V9155qE9EfWqKbWao/A+RGV/mbrguPwIX
BexBiu7GTjGUAsMpb129qUotwNfdLMawjFZ8TXq7HXewajspIiwpirbxWC9SqIRW
qKE6gxkxMcY+jhBatJr7nvrrHBRDx0EeL6MimDoeCDZZn1UNt03EqTin4gGPPtEa
fH1sLfcrDSOH3TSQb2X4K3jntstH/daV8KhKP2E2O3vvR2H58q9qpUxRGFspCduD
W0cL+/NBd168rag/nDugCGr0qUSrkqpNlL+fDDosfBHh8Qy0joDtnOiq4wdw0vMi
waUC2x6edw4GKAvC/elKtnK9WRH5NoWa5p9PY4H5i5AWXmii5IbMIHqr25nUvAL9
sS7NcdOuFvR05k9dce8kroj0UHxdU2LrcMGGpzS770hXjkZSrCGtzN2WZQARAQAB
tBtHaXRMYWIgQ1ZFIDxjdmVAZ2l0bGFiLmNvbT6JAlQEEwEKAD4WIQSN7uA0VTv5
j3YMqdL6be/LIZJiyAUCXrneqAIbAwUJAeEzgAULCQgHAgYVCgkICwIEFgIDAQIe
AQIXgAAKCRD6be/LIZJiyHosD/91MwTEl36TLMxdJqMVipPlwIFDFPUNoxn/OwBQ
GxdVm5WtbJhMaSHd5TMZUvhKMYqUpFNVmvESxwn5q8Tc74ZcADzhAsWK8z5xbDPD
l6VhPWO6aESrlUGLNrpzJCT7Edy6fKX4t5tYe3JRcZ2EpjgC1NlHzZbvo+sCwEb+
KhxlOXLrqkzuzs/MIerjy4v3heifcVVtZnP1Lps6Gl9PhyXXuHoXXhqsGgepJyJO
O+4SYo0WfocqeE23PFfZ0+RNHnOtvshb1saoo2nUl+u4MxqskUHSbWRS0rHeEJuj
YwEIiK/uCNkTqzuPzV7zD6lGJdpzopmgL3Po6WoWsM4vTt4WstzLwy2VUFJtMgqr
uMJYUw+G+UfHN5+YTM7+PV70kvlxvZ3Kj/iOqGx2BBWNeQBiC/TWhZxc7uRj5fHg
qpM1omHghmOWCh9q/GzfP6qSl1f2Fri+4qcHutciBWWgV7qfTQWa4f/h4PbuTbaa
KjZ6ITFGYsEfZSwQAClyKPN5QhJ5AF4csYRQOOr4+MLh3E3R6AvrjD0cdt7ewdCx
FSTwDm7YVCF2Sd36Bu8BFn5IxyayZtq1sRC93EnWPHyl8HiHBiV7fhOQhvi+J0bu
ca1HibqtI4cLXzx5b57oLxAZGIedyTJeMSt+apJd/G+j4qA02gUoedpMO7KLYJoa
44TWx7kCDQReud6oARAAx52wVpt9TzMbFOWzL+/yPjn01BlrssbQ/XZsDPUzgZGL
cRy55AtivBPhFl83KvgDXJaEmpwf5eA17jk3Az617Uz2MPTbXIVKddTS2d2DTsBd
dCsw7uAMjv6naJVSLVwFcPsgkpYSaWcegFjmOfBBFZnF+WL45yqFLk1E/Ek67ciB
/cQ32pPgce5+p/QZACyzWnlWrzPCyJJYKItFb160lXajpKLgT5X9b7dQ8M1G7oTw
rEaafxT3Mt+ICSlfWFZqd+cjLGswEfTtKF9jJJ/3GJw8MKgW3NFjEqt1U6HVNfZf
PfWOBe3pxlh9yS3d+1DGX/ornZKB3w1F7z4IUQAiMJZM+8VBQRYvrjLuyGlwhjTd
AiDzSDSvbNixP/BMdoZ//bZgej6FU711wqwMPqlIQS6WbyBrE6LmXjll/mahjgF0
jhDuBqgY8Sw3qLzcIWD1LGATeKVdNLpbVoUWwE9+J9XMNanVAtEZqD7AYxu24pVN
eoMLdrrfqt+HoLl8ziy3Ib3DYeNP/TkZH8talxJtq5wZ9E74PRtxk0XkVBjUEnFV
TjwYIkuOde1MJ8Xy1tzGSz0jFGP6ivom/DeK2Cm/hOroc8KW7s3HhSI+XJ7oclWU
buIzgkMtz5op9xofQ05lMw4kymghl9F5TvZm8AUd0H5GwWEv+SEF8JzeZ1m4WwMA
EQEAAYkCPAQYAQoAJhYhBI3u4DRVO/mPdgyp0vpt78shkmLIBQJeud6oAhsMBQkB
4TOAAAoJEPpt78shkmLI3wEQAJNHo9dBQNOX2/ehKWkwADdHFf48zwHvQCEK+KW6
Yl7xTRadIESzenRp+x5vCnwLe52FAjtHrW2q856/PQT3xQUHx26txsGIBFwCijrp
/jM32nM5scYI8yzu7GAteJuZC+FXWdHqzPjSJyf7fI4IEY3fNfnuusBUFKIiVupY
OP+PrZsZl8HwuL3Ojp9O/TWWzg7z/aA/JbL8JYz7RpLfk5pPCt+2yzXvoLCaIv+0
p6a8lm74i7fXtYdzK83wS4tT2AcPNGA4pGtOpSs8I5O0FxTGNiMXqV4qkt3VUgOn
qlJ1lXskM5BmyjajyWcyzn484kY+IQ4kjnli6ZNWHBGdVF9RCL8PIIttZsL2D6TS
mPhQSVdlWiMTexM1BxEtHbS5Q4G+hkhuU80bP4iwbz3sSO8+v+expI9CIRFrmZMI
2hwfpqKspK/z9meg4olnjUMOTU4OXu2TNwfUuYFJcwJ4IrDjAguKJXSa4fVTuh8W
e+JvhzpPqj0olU9TqeCSt79BZtm+nX352lOrl4YjwKY52oN9BNPDK/lf4u6HFkCw
s0YiVJ+Ch0vKVuWJaXKBx8zuHkZAc1/ADV9pIYZo1psJzJiONhQrwX+QsyWVDMFx
x07fbuhepzlhXvHT5GW5wTvRSnaDRUaYA2QTxLJTvUGSg9cOY1SGvdIvgVFVTPUO
C7kX
=LSYR
-----END PGP PUBLIC KEY BLOCK-----
```
