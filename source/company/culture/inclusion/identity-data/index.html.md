---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-07-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 138       | 10.71%          |
| Based in EMEA                               | 345       | 26.76%          |
| Based in LATAM                              | 21        | 1.63%           |
| Based in NORAM                              | 785       | 60.90%          |
| **Total Team Members**                      | **1,289** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 897       | 69.59%          |
| Women                                       | 392       | 30.41%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,289** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 72        | 75.79%          |
| Women in Leadership                         | 23        | 24.21%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **95**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 441       | 81.22%          |
| Women in Development                        | 102       | 18.78%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **543**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.27%           |
| Asian                                       | 48        | 6.52%           |
| Black or African American                   | 20        | 2.72%           |
| Hispanic or Latino                          | 40        | 5.43%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 4.08%           |
| White                                       | 449       | 61.01%          |
| Unreported                                  | 147       | 19.97%          |
| **Total Team Members**                      | **736**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 17        | 7.94%           |
| Black or African American                   | 4         | 1.87%           |
| Hispanic or Latino                          | 8         | 3.74%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.21%           |
| White                                       | 139       | 64.95%          |
| Unreported                                  | 37        | 17.29%          |
| **Total Team Members**                      | **214**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 11.84%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 5.26%           |
| White                                       | 48        | 63.16%          |
| Unreported                                  | 15        | 19.74%          |
| **Total Team Members**                      | **76**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 116       | 9.00%           |
| Black or African American                   | 33        | 2.56%           |
| Hispanic or Latino                          | 66        | 5.12%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 38        | 2.95%           |
| White                                       | 725       | 56.25%          |
| Unreported                                  | 309       | 23.97%          |
| **Total Team Members**                      | **1,289** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 54        | 9.94%           |
| Black or African American                   | 10        | 1.84%           |
| Hispanic or Latino                          | 26        | 4.79%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 15        | 2.76%           |
| White                                       | 308       | 56.72%          |
| Unreported                                  | 130       | 23.94%          |
| **Total Team Members**                      | **543**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.53%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.05%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 4.21%           |
| White                                       | 57        | 60.00%          |
| Unreported                                  | 23        | 24.21%          |
| **Total Team Members**                      | **95**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.40%           |
| 25-29                                       | 224       | 17.38%          |
| 30-34                                       | 349       | 27.08%          |
| 35-39                                       | 295       | 22.89%          |
| 40-49                                       | 278       | 21.57%          |
| 50-59                                       | 110       | 8.53%           |
| 60+                                         | 15        | 1.16%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,289** | **100%**        |


Source: GitLab's HRIS, BambooHR
