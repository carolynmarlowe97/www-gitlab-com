---
layout: handbook-page-toc
title: "Merchandise Workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Requesting Swag

This page outlines ways that GitLab team members can request swag. If you are looking to host a giveaway of GitLab swag, please review the [GitLab Giveaways process](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/giveaways/).

### Important Notes

We recommend that you request merchandise **at least 4 weeks in advance** for us to be able to accommodate your request. If your request is urgent, please reach out to the community advocates via <merch@gitlab.com> to find out if an expediated shipping option is available.

Other ways you can get support for your swag request:
* Tag the @advocates in the [#swag Slack channel](https://app.slack.com/client/T02592416/C66R8N98F/thread/CB16DMSLC-1594317317.394500)
* In order to keep the orders transparent, please do not send requests via direct messages. Instead, email <merch@gitlab.com>.


## Types of Swag Requests

GitLab team members can use the Unfiltered Youtube Channel to watch the following training: [Swag Requests](https://youtu.be/l36QBqHn0rE)

Please use this table to confirm you're using the correct order process for GitLab Swag:

| Swag Order Scenario | Order Process|
| --- | --- |
| You're speaking at an upcoming event and would like to bring GitLab swag | Follow the [Swag for Speaking at or Hosting GitLab events process](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/swag-requests/#swag-for-speaking-at-or-hosting-gitlab-events) |
| You're hosting an event and you'd like to share GitLab swag | Follow the [Swag for Speaking at or Hosting GitLab events process](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/swag-requests/#swag-for-speaking-at-or-hosting-gitlab-events) |
| You're ordering swag for a customer | Use the [Printfection Sales Swag link](https://get.printfection.com/dcdzm/6508378270). This link is for GitLab internal use only and requires login with a @gitlab.com email address. If you have trouble signing into Printfection, ping the #swag slack channel for support. Review the [Swag for customer/ prospects](/handbook/marketing/corporate-marketing/#swag-for-customer-prospects) for guidelines on order sizes for customers. |
| You want to recognize a wider community member for their contributions | Submit the form for [GitLab Nominations for Community Swag](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/community-rewards-internal/) |
| You want to organize a giveaway | Follow the [Giveaway process](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/giveaways/) |
| You want to recognize an MVP on release day | Follow the [MVP appreciation gifts process]((/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/swag-requests/#mvp-appreciation-gifts)


### Swag for Speaking at or Hosting GitLab Events

GitLab team members can request a merch order for an event they are speaking at or hosting by emailing the request to <merch@gitlab.com>.

Please use the following email template when placing a merchandise request:

```markdown
Hello Merch Team,

I'd like to place the following GitLab merchandise order:

Reason for Order: 

Item Name: 
Size, if applicable: 
Quantity:


Name for the order:
Shipping Address:
Email Address:
Phone Number:


Regards,
YOUR_NAME
```


### MVP Appreciation Gifts

Each 22nd of the month is a release day - every release we pick a Most Valuable Person and thank them for their contributions. We send them some GitLab swag as a thank you (e.g. a hoodie, socks, and a handmade tanuki). There's also the option of sending personalized swag - see [custom swag providers](#good-custom-swag-providers).

1. Determine MVP after merge window closes, see `#release-post` channel
1. Find MVP's contact information
  * An email address is usually stored in git commit data
  * A user might have email or twitter info on their profile
1. Congratulate the MVP via email, ask for their shipping address, as well as any other relevant information (e.g. shirt size)
1. Investigate the MVP's interests
  * If the MVP doesn't have a notable presence on social media, you may choose to ask them directly or send GitLab swag instead
1. Choose a suitable gift (up to 200$ USD)
1. Write a kind thank you message
1. Send the gift
  * The MVP should ideally have the gift 48h before the post goes live, though shipping to people outside the United States can take longer and usually won't make it in time
1. Verify shipment status
  * Make sure that it was sent
  * Make sure that it arrived
1. Mention the MVP gift in the release post
  * Make sure there's a picture of the gift in the release post if it's available
