---
layout: handbook-page-toc
title: License and Renewal Queue Workflow
category: License and subscription
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This details how GitLab Support works the License and Renewals queue (L&R) and
how other teams might escalate those tickets should it be needed.

## What is the queue for?

The L&R queue would be for customer tickets relating to issues with licensing
and renewals. This might include topics like:

* How to apply a license
* Errors when applying a license
* Generalized license queries from a user

For issues relating to the product that handles licensing, those would be
directed to the `#g_fulfillment` channel in slack.

## What is this queue **not** for?

The L&R queue should not be used for the following:

* Billing related matters, such as payments, invoice generation, refunds, etc.
* Product related questions
* [New business requests](https://about.gitlab.com/sales/)
* Requests related to the
  [education program](https://about.gitlab.com/solutions/education/)
* Requests related to the
  [open source program](https://about.gitlab.com/solutions/open-source/program/)

## Workflow

To begin working the queue, you would first want to complete the Growth
Specialty Bootcamp. Once you have completed that, you will gain mastery in this
area, thus signaling your readiness to work the queue.

Generally speaking, 
[Subscription and billing issues](license_troubleshooting.html) will cover
the majority of the issues you will encounter. For those not detailed there, it
is recommended to either talk to an Escalation Point about the issue or make a
MR to add the details to that page.

## Escalation Points

For issues that go outside the norm, you would want to reach out to one of our
L&R queue escalation points:

* Tom H (AMER)
* Donique (EMEA)
* Rotanak (APAC)

These agents are trained in handling the non-standard L&R related issues and
can help work through them.

## Where do licensing questions go?

For generalized license related questions, you can ask via the `#questions`
channel in slack. 

If the question pertains to a specific customer issue and the circumstances are such that you do not want to ask the customer to [open a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293), you can [open an internal issue](https://gitlab.com/gitlab-com/support/internal-requests/issues/new?issuable_template=Plan%20Change%20Request) using the `License Issue` template for assistance.

## What is the SLA for a L&R ticket?

Currently, the SLA for these tickets is 24 business hours.

## How/when do I escalate a L&R ticket?

Currently, there is only one real criteria for escalating a L&R ticket:

* The ticket will breach SLA within the hour.

Outside of this, the ticket is usually able to wait until one of our agent's is
able to work through the ticket.

To escalate a L&R ticket, you would want to follow Support's existing
[escalation policy](/handbook/support/internal-support/#i-want-to-draw-attention-to-an-existing-support-ticket).
