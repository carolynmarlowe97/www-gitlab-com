---
layout: handbook-page-toc
title: "Data Team Calendar"
description: "GitLab Data Team Calendar"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## Meetings 

The [Data Team's Google Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9kN2RsNDU3ZnJyOHA1OHBuM2s2M2VidW84b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) is the SSOT for meetings.
It also includes relevant events in the data space.
Anyone can add events to it.
Many of the events on this calendar, including Monthly Key Reviews, do not require attendance and are FYI events.
When creating an event for the entire Data Team, it might be helpful to check their working hours in Google Calendar and discuss out of working hour meetings ahead of scheduling. Please consider alternating who is meeting after working hours when such meetings are necessary.

### Meeting Tuesday 

The team honors *Meeting Tuesday*.
We aim to consolidate all of our meetings into Tuesday, since most team members identify more strongly with the [Maker's Schedule over the Manager's Schedule](http://www.paulgraham.com/makersschedule.html).


### Recurring Meetings

**Weekly Meetings**

| Meeting | Weekday | Time (UTC) | Description | 
| ----- | ----- | ----- | ----- |
| [Data Analyst Team Meeting](https://docs.google.com/document/d/1jOBjCY9-Fp655byvyaPom5Y8a0vH8xvdRrsxz95xfdU/edit?usp=sharing) | Tuesday | 1430 | Bi-weekly check-in and Ad Hoc team topics |
| [OKR Sync](https://docs.google.com/document/d/1Lkx8uNoOFL1hpk0EiWmQp4xexyShJYqMigIxASG5xVM/edit?usp=sharing) | Tuesday | 1500 | <!-- Needs description --> |
| [Data Engineering Team Meeting](https://docs.google.com/document/d/1J6yPgnEpr2VXz2LFXNNA4w39UBaibtBrx8qE69H_xQY/edit) | Tuesday | 1500 | Milestone planning and Ad Hoc team topics |
| Social Call  | Tuesday | 1700 | No agenda; 30 minutes to catch up on life. |
| [Release to Adoption Fusion Team Meeting](https://docs.google.com/document/d/1cANv4hvrIt2V66yRJztzb_2tuDYOx1ashEzD2G69NbE/edit) | Tuesday | 1900 | Milestone planning and Ad Hoc team topics. |

**Monthly Meetings**

* [Monthly Data Team All-Hands](https://docs.google.com/document/d/1CsTVyNPIrdHYz7N6ezCDo_ysXnjCvQlnv89vRbKCjNI/edit?usp=sharing)

### Daily Standup

Members of the data team use Geekbot for our daily standups.
These are posted in [#data-daily](https://gitlab.slack.com/archives/CGG0VRJJ0/p1553619142000700).
When Geekbot asks, "What are you planning on working on today? Any blockers?" try answering with specific details, so that teammates can proactively unblock you.
Instead of "working on Salesforce stuff", consider "Adding Opportunity Owners for the `sfdc_opportunity_xf` model."
There is no pressure to respond to Geekbot as soon as it messages you.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.
