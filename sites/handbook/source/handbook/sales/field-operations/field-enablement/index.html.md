---
layout: handbook-page-toc
title: "Field Enablement"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The Field Enablement team's mission is to help customers successfully grow and evolve in their journey with GitLab to achieve positive business outcomes with effective enablement solutions aligned to [Gitlab’s values](/handbook/values/). 

How we do this:
*  Collaborate with cross-functional stakeholders (including but not limited to Sales, Customer Success, Marketing, Sales Ops, Product/Engineering, and the People team) to address prioritized knowledge, behavior, and skill gaps in the GitLab Field Team to achieve desired business outcomes.
*  Define, coordinate, and/or lead the development and delivery of effective enablement solutions (training, technology, knowledge, content, process, and tools) for GitLab Sales, Customer Success, and Partners throughout all phases of the customer journey as well as different stages of each team member's tenure and/or experience at or with GitLab.
*  Champion the needs and interests of the Field Team (all routes to market) – ensuring that they are regularly informed about relevant business and organizational updates and have access to enablement materials necessary for their day-to-day work to meet and exceed business objectives.
*  Cultivate a culture of curiosity, iteration, and continuous learning & development across the GitLab field organization.

Please see our [Key Programs](/handbook/sales/field-operations/field-enablement/#key-programs) to learn more. Want to communicate with us? Please Slack us at [#field-enablement-team](https://gitlab.slack.com/archives/field-enablement-team).

## Strategy

<!-- blank line -->
<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQGUQ9g98p31bPZ267en01qJCqgjeX8ZC6GmChBTKz7TV0OEwhFlKbXPgf1YARh5V-lDBegFpu60iTL/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>
<!-- blank line -->

## Key Programs

*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [Command of the Message](/handbook/sales/command-of-the-message)
*  Continuous Learning
    *  [Customer Success Skills Exchange](/handbook/sales/training/customer-success-skills-exchange)
    *  [Sales Enablement Level Up Webcast Series](/handbook/sales/training/sales-enablement-sessions/)
    *  [Sales Training Resources](/handbook/sales/training/)
    *  [Technical Questions for Sales](/handbook/sales/training/technical-questions-for-sales/)
*  [Field Certification Program](/handbook/sales/training/field-certification)
    *  [Sales Operating Procedures](/handbook/sales/sales-operating-procedures/)
    *  [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/)
*  [Sales Manager Best Practices](/handbook/sales/field-operations/field-enablement/sales-manager-best-practices)
*  [Field Communications](/handbook/sales/field-communications/)
   *  [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/)
   *  [Monthly GitLab Release Email to Sales](/handbook/sales/field-communications/monthly-release-sales-email)
*  Sales Events
    *  [Sales Kick Off](/handbook/sales/training/SKO)
    *  [Sales QBRs](/handbook/sales/#quarterly-business-reviews)
    *  [GitLab President's Club](/handbook/sales/club/)

## Handbook-First Approach to GitLab Learning and Development Materials

Chat between David Somers (Sr. Director, Field Enablement) and Sid Sijbrandij (CEO)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

<details>
<summary markdown="span">Key Discussion Points</summary>

- Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
- We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission).
- There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
   * Folks who have already completed a formal training through an e-learning platform may want to return to the materials
   * Those who never go through the formal platform may also benefit from the materials
   * The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more
</details>

*  Learn how Field Enablement takes a [Handbook-first Approach for Interactive Learning](/handbook/sales/field-operations/field-enablement/interactive-learning/)

## Six Critical Questions
Inspired by _The Advantage: Why Organizational Health Trumps Everything Else in Business_ by Patrick Lencioni

<details>
<summary markdown="span">1. Why does the GitLab Field Enablement team exist?</summary>
See our team mission in the [Overview](/handbook/sales/field-operations/field-enablement/#overview) section.
</details>

<details>
<summary markdown="span">2. How do we behave?</summary>
On our best day, we show up with a positive attitude while demonstrating [GitLab’s values](/handbook/values/) along with the following behaviors to overcome the [Five Dysfunctions](/handbook/values/#five-dysfunctions):

- **Trust**: Extend trust, actively listen, and assume noble intent; give and receive feedback with respect and solicit feedback often
- **Embrace Healthy Conflict**: Engage in constructive conflict for the purpose of achieving shared goals & objectives; resolve personal issues, quickly and directly
- **Commitment**: Support decisions once decisions are made with a GitLab team-first approach
- **Accountability**: Hold ourselves and each other accountable while encouraging each other & celebrating successes
- **Results**: Strong drive for results and a focus on the customer; demonstrate passion for continuous learning & improvement
</details>

<details>
<summary markdown="span">3. What does the Field Enablement team do?</summary>
See "How we do this" in the [Overview](/handbook/sales/field-operations/field-enablement/#overview) section.
</details>

<details>
<summary markdown="span">4. What does success look like?</summary>
The below is a work in progress as we define success measures across each stage of the customer journey:

- **Engage & Educate the Customer**
    - Increase # of rep-sourced opps
    - Accelerate sales cycle time and improve conversion of MQLs to SAOs
    - Accelerate and improve predictability of new rep ramp time
- **Progress the Opportunity & Close the Deal**
    - Increase # of closed deals per rep
    - Accelerate sales cycle time and improve conversion of SAOs to Closed/Won deals
    - Increase average sale price (inclusive of improved product mix to sell more Premium/Silver and Ultimate/Gold)
    - Accelerate and improve predictability of new rep ramp time
    - Improve forecasting accuracy
    - Improve win rates
- **Retain & Expand**
    - Improve renewal rates (inclusive of up-sell and cross-sell)
    - Accelerate customer time to value
    - Increase breadth of stage adoption
</details>

<details>
<summary markdown="span">5. What is most important right now (2HFY21)?</summary>

1. Improve field efficiency by training and sustaining prioritized knowledge and behavior/skill gaps
    - Field certification program (Sales, CS & Partner audiences)
         - Product feature, tiering, and customer use cases
         - Moments That Matter across the customer journey
         - Continue to operationalize Command of the Message & MEDDPPICC
    - Deliver enablement program aligned with top competitor GTM initiatives
    - Embrace partner enablement as an extension of GitLab Sales and Customer Success enablement
1. Promote a culture of continuous learning and development
    - Improve learning experience by implementing new Learning Experience Platform (LXP) 
    - Develop and execute robust change management plan that taps into team members’ inherent desire to pursue excellence
    - Continued execution & ongoing iteration of functional onboarding training to accelerate time to productivity for new field team members
    - Continued execution & ongoing iteration of continuous learning programs (CS Skills Exchange and Sales Enablement Level Up Webcast series)
1. Ensure that the GitLab field organization is regularly informed about relevant business and organizational updates and has access to information & resources necessary for their day-to-day work
    - Continue to mature and advance field communications strategy
    - Make it easier for Sales to discover & access information/resources they need when they need it
    - Keep planning for SKO 2021 on track
</details>

<details>
<summary markdown="span">6. Who must do what?</summary>

- **[Sr. Director, Field Enablement](/job-families/sales/director-of-field-enablement/#senior-director-field-enablement)**
    - [David Somers](/company/team/#dcsomers)
- **[Program Managers: Enterprise Sales, Commercial Sales, and Technical Sales / Customer Success](/job-families/sales/program-manager-field-enablement/)**
    - [Eric Brown](/company/team/#ejbrown71) (Enterprise Sales Enablement)
    - [Tanuja Paruchuri](/company/team/#tparuchuri) (Sales & Customer Success Onboarding)
    - [Kris Reynolds](/company/team/#kreynolds1) (Customer Success Enablement)
    - [Kelley Shirazi](/company/team/#kelley-shirazi) (Commercial Sales Enablement)
    - TBH (Partner Enablement)
- **[Sales Training Facilitator](/job-families/sales/sales-training-facilitator-field-enablement/)**
    - [John Blevins](/company/team/#jblevins608)
- **[Sales Communications Manager](/job-families/sales/sales-communications-manager/)**
    - [Monica Jacob](/company/team/#monicaj)
- **[Technical Instructional Designer](/job-families/sales/technical-instructional-designer/)**
    - [Issac Abbasi](/company/team/#iabbasi)
</details>   


## Field Enablement groups, projects, and labels
   *  **Groups**
      - Use the GitLab.com group for epics that may include issues within and outside the Sales Team group
      - Use the GitLab Sales Team group for epics that may include issues within and outside the Field Operations group
   *  **Projects**
      - Create issues under the “Enablement” project
   *  **Labels**
      - **Team labels**
          - `field enablement` - issue initially created, used in templates, the starting point for any label that involved Field Enablement
          - `FieldOps` - label for issues that we want to expose to the VP of Field Operations; these will often mirror issues with the SCE: priority 1 tag, particularly those relating to OKRs and other prioritized projects by Field Ops leadership
      - **Stakeholder/Customer labels**
          - `FE:CS enablement` - label for Field Enablement issues related to enabling Customer Success (CS) roles
          - `FE:sales enablement` - label for Field Enablement issues related to enabling Sales roles
          - `FE:partner enablement` - label for Field Enablement issues related to enabling Partners
      - **Initiative labels**
          - `continuing education` - label for issues related to continuing education
          - `career dev` - label for issues related to career development
          - `field certification` - label for issues related to /handbook/sales/training/field-certification/
          - `field communications` - label for items that include work by/with the Field Communications team within Field Enablement
          - `field events` - label for Field Enablement-supported events (e.g. QBRs, SKO, President's Club, etc.)
          - `force management` - label for issues related to Force Management engagement
             - `vff` - label for Value Framework Feedback
             - `vff::new` - starting point for Value Framework feedback
             - `vff::accepted` = Value Framework feedback that will be actioned on
             - `vff::deferred` = Value Framework feedback that will be deferred until more information is gathered
             - `vff::declined` = Value Framework feedback that is declined (no action will be taken)
          - `onboarding` - label for issues related to onboarding
          - `sales enablement sessions` - label for weekly virtual sales enablement series
          - `sko` - label for issues related to Sales Kick Off
          - `status:plan` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized but not yet scheduled
          - `status:scheduled` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized and scheduled
      - **Priority labels**
          - `FE:new request` - label for triaging new requests originating outside of the Field Enablement team
          - `FE priority::1` - Home runs (high value to GitLab and high likelihood of success that align to S&CE OKRs) and committed to completion within 90 days. This category will be limited because not everything can be a priority.
          - `FE priority::2` - Big Bets (high value to GitLab, lower likelihood of success) within 90 days
          - `FE priority::3` - Small wins (lower value to GitLab, high likelihood of success) within 90 days
          - `FE priority::Backlog` - Things in the queue not currently being worked
          - `QBR` - Requests from Sales QBRs
   *  **Field Enablement Issue Boards**
      - [Field Enablement By Initiative Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1191445)
      - [Field Enablement By Priority Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1644552?&label_name[]=field%20enablement)
      - [Sales Enablement Sessions Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1231617)
      - [Customer Success Skills Exchange Board](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/boards/1414538)
      - [Sales & CS Onboarding Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1645038)
